#ifndef ARETE_H_INCLUDED
#define ARETE_H_INCLUDED

#include <iostream>
#include <iomanip>
#include "sommet.h"


/*!
 * \file arete.h
 * \brief  class arete
 * \author Emilie - Lizzie - Louis
 * \version FINAL
 */


/*! \class Arete
* \brief classe representant l'ensemble des aretes du graphe
*/

class Arete
{
    public :

        /*!
        * \brief constructeur d'arete
        * \param prend en parametre tous les attributs de sommet
        */
        Arete (int id, int idsommet1, int idsommet2);

        /*!
        * \brief constructeur d'arete
        * \param prend en parametre tous les attributs de sommet
        */
        Arete (int id, int idsommet1, int idsommet2, std::vector <float> poids);

        /*!
        * \brief destructeur d'arete
        */
        ~Arete();

        /*!
        * \return le poid d'une arete
        */
        inline float GetPoids(int num_poids){return m_poids[num_poids];};

        /*!
        * \return une pair de sommet
        */
        inline std::pair<int, int> getSommets(){return {m_id_sommet_ini, m_id_sommet_fini};};

        /*!
        * \param le poid d'une arete
        */
        void setPoids(std::vector <float> poids);

        /*!
        * \return l'id de l'arete
        */
        inline int getId(){return m_id;};


        /*!
        * \brief affiche une arete
        */
        void afficher() const;

        /*!
        * \brief affiche l'id de l'arete
        */
        void afficherId() const;

        /*!
        * \brief dessine une arete
        * \param  Svgfile& svgout, std::vector<Sommet*> m_sommets, double decalage_x, double decalage_y
        */
        void dessinerArete(Svgfile& svgout, std::vector<Sommet*> m_sommets, double decalage_x, double decalage_y);

        /*!
        * \brif dessiner les aretes avec le poids
        * \param Svgfile& svgout, std::vector<Sommet*> m_sommets, double decalage_x, double decalage_y
        */
        void dessinerAreteAvecPoids(Svgfile& svgout, std::vector<Sommet*> m_sommets, double decalage_x, double decalage_y);

        /*!
        * \brief transforme un float en string
        * \param float
        * \return string
        */
        std::string float2string(float f);
        void setBool(bool marq);
        bool getBool();

        int getIdSommetIni();
        int getIdSommetFin();

        inline int getPond() {return m_poids.size();};

    private :
        int m_id;
        int m_id_sommet_ini;
        int m_id_sommet_fini;
        std::vector <float> m_poids; //pour pouvoir gerer d'autre valeur
        bool m_marq = false;
};

#endif // ARETE_H_INCLUDED
