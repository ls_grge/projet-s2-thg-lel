#include <iostream>
#include <queue>
#include <stack>
#include<unordered_map>
#include<unordered_set>
#include "sommet.h"

Sommet::Sommet(int id, Coords coordonnees):
    m_id{id}, m_coords{coordonnees}
{ }

void Sommet::afficherData() const
{
     std::cout<<"    "<<m_id<<std::endl;
     m_coords.afficher();
}

void Sommet::afficherVoisins() const{
    std::cout<<"  voisins :";
    for(auto v:m_voisins)
        std::cout << "  id  " << v.first->getId() << " ; ";
}

void Sommet::setVoisins(Sommet* voisin){
    m_voisins.push_back({voisin, 0.0 });
}

void Sommet::setPoids(int id_voisin, float poids2){
    for(unsigned int i=0; i<m_voisins.size(); ++i)
        if ((m_voisins[i].first->getId()) == id_voisin)
            m_voisins[i].second=poids2;
}

float Sommet::getPoids2(int id_voisin){
    for(unsigned int i=0; i<m_voisins.size(); ++i)
     if ((m_voisins[i].first->getId()) == id_voisin)
            return (m_voisins[i].second);

    return 0;
}

void Sommet::setVP(Sommet* voisin, float poids2){
    m_voisins.push_back({voisin, poids2 });
}

Coords Sommet::getCoords()
{
    return m_coords;
}

std::unordered_map<int ,int > Sommet::parcoursDFS() const{
    ///creation d'une map vide o� vont �tre stocke les couples d'indentifiants (sommet, predecesseur)
    std::unordered_map<int ,int > l_pred;
    ///cr�ation d'une pile vide qui va contenir les sommets
    std::stack<const Sommet*> pile;
    ///declaration d'une variable sommet
    const Sommet* s;

    ///on stocke le sommet initial
    pile.push(this);;

    while(!pile.empty()){
        s=pile.top(); ///on recup�re le sommet en t�te
        pile.pop(); ///on le supprime

        for(size_t i=0; i<s->m_voisins.size(); ++i){
            if((l_pred.find(s->m_voisins[i].first->m_id)==l_pred.end())&&(s->m_voisins[i].first->m_id!=this->m_id)){
                pile.push(s->m_voisins[i].first);
                l_pred.insert({s->m_voisins[i].first->m_id, s->m_id});
            }
        }
    }

    return l_pred;
}

void Sommet::dessinerSommet(Svgfile& svgout, double decalage_x, double decalage_y)
{
    svgout.addDisk(m_coords.getX() + decalage_x , m_coords.getY() + decalage_y, 7, "black");
    svgout.addText(m_coords.getX()+ 7 + decalage_x, m_coords.getY()- 7 + decalage_y, std::to_string (m_id), "violet");
}

std::unordered_set<int> Sommet::rechercherCC() {
    std::unordered_set<int> cc;
    std::unordered_map<int,int> l_pred = parcoursDFS();

    cc.insert(m_id);
    for (auto s:l_pred) cc.insert(s.first);

    return cc;
}

Sommet::~Sommet()
{
    //dtor
}

int Sommet::getId() const {
    return m_id;
}

std::unordered_map<Sommet*,std::pair<Sommet*, float>>Sommet::algoDijkstra() {
    ///creation d'une map vide o� vont �tre stocke les couples d'indentifiants (sommet, predecesseur, distance)
    std::unordered_map<Sommet*,std::pair<Sommet*, float>> l_pred;
    ///cr�ation d'une liste vide qui va contenir les sommets marqu�s
    std::unordered_set< Sommet*> OK;
    ///cr�ation d'une liste ordonn�e des distances depuis s0 associ�es aux sommets
    ///fonction de comparaison + d�claration
    auto cmp = [](std::pair<Sommet*, float> p1, std::pair<Sommet*, float> p2) {return p2.second<p1.second;};
    std::priority_queue< std::pair< Sommet*, float>, std::vector<std::pair<Sommet*, float>>, decltype(cmp)> fileP(cmp);

    ///on stocke le sommet initial et sa distance
    fileP.push({this, 0});

    while(!fileP.empty()){

        ///on r�cup�re le sommet prioritaire et on le supprime dans la file de priorit�
        std::pair<Sommet*, float> p= fileP.top();
        fileP.pop();

        ///ajout � la liste des sommets marqu�s
        OK.insert(p.first);

        for(size_t i=0; i<p.first->m_voisins.size(); ++i){

            ///s'il le sommet successeur n'existe pas dans la liste des sommets d�couverts
            if((l_pred.find(p.first->m_voisins[i].first)==l_pred.end())&&(p.first->m_voisins[i].first!=this)){

                ///ajout dans la file de priorit� du sommet d�couvert
                fileP.push({p.first->m_voisins[i].first, p.second + p.first->m_voisins[i].second});
                ///ajout dans la liste des sommets d�couverts
                l_pred.insert({p.first->m_voisins[i].first, {p.first, p.second + p.first->m_voisins[i].second}});
            }
            else if(l_pred.find(p.first->m_voisins[i].first)!=l_pred.end()) {

                ///s'il y est : v�rification + MAJ
                if(l_pred.find(p.first->m_voisins[i].first)->second.second > (p.second + p.first->m_voisins[i].second)){
                    l_pred.find(p.first->m_voisins[i].first)->second.first = p.first;
                    l_pred.find(p.first->m_voisins[i].first)->second.second = (p.second + p.first->m_voisins[i].second);

                    ///ajout dans la file de priorit� du sommet d�couvert
                    fileP.push({p.first->m_voisins[i].first, p.second + p.first->m_voisins[i].second});
                }
            }
        }
    }
    return l_pred;
}

void Sommet::setBool(bool marq) {
    m_marq=marq;
}

bool Sommet::getBool() {
    return m_marq;
}
