#ifndef SOMMET_H_INCLUDED
#define SOMMET_H_INCLUDED

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include "coords.h"
#include "svgfile.h"



/*!
 * \file sommet.h
 * \brief  class sommet
 * \author �milie - Lizzie - Louis
 * \version FINAL
 */


/*! \class Sommet
* \brief classe representant l'ensemble du graphe
*/

class Sommet
{
    public:
        /*!
        * \brief constructeur qui re�oit en params les donn�es du sommet
        * \param prend un id : en int
        * \param des coordonnees
        */
        Sommet(int id, Coords coordonnees);

        /*!
        * \brief affiche les data du sommet
        */
        void afficherData() const;

        /*!
        * \brief affiche les voisins du sommet
        */
        void afficherVoisins() const;

        /*!
        * \brief m�thode de parcours en profondeur du graphe � partir du sommet
        * \return renvoit une map de int
        */
        std::unordered_map<int,int> parcoursDFS() const;

        /*!
        * \brief algorithme de Dijkstra du graphe � partir du sommet
        * \return map de sommet* et pair de sommet float
        */
        std::unordered_map<Sommet*,std::pair<Sommet*, float>> algoDijkstra();
        /*!
        * \brief m�thode qui recherche la composante connexe du sommet
        * renvoie la liste des ids des sommets de la composante
        * \return unordered set de int
        */
        std::unordered_set<int> rechercherCC() ;

        /*!
        * \brief destructeur de sommet
        */
        ~Sommet();

        /*!
        * \brief retourn l'id du sommet
        * \return un int
        */
        int getId() const ;

        /*!
        * \param int : demande quel poid renvoyer
        * \return renvoit le poids : float
        */
        float getPoids2(int);

        /*!
        * \brief dessine en svg les sommets
        * \param svgout :  objet de svgfile pour le svg
        * \param decalage_x decalage_y : servent pour la position des sommet sur le dessin
        */
        void dessinerSommet(Svgfile& svgout, double decalage_x, double decalage_y);

        /*!
        * \return les coordonnees du sommet
        */
        Coords getCoords();


        /*!
        * \param set la marque du sommet
        */
        void setBool(bool marq);


        /*!
        * \return la marque du sommet
        */
        bool getBool();


        /*!
        * \param set les sommet voisins
        */
        void setVoisins(Sommet*);


        /*!
        * \param  set le poid des sommets
        */
        void setPoids(int ,float); ///en fonction du cout 2 uniquement


        /*!
        * \param sommet* et float 
        */
        void setVP(Sommet*, float);

    protected:

    private:
        /// Voisinage : liste d'adjacence
        std::vector<std::pair<Sommet* , float>> m_voisins;

        /// Donn�es sp�cifiques du sommet
        int m_id; // Identifiant
        Coords m_coords; // Coordonnees
        bool m_marq = false;

};

#endif // SOMMET_H_INCLUDED
