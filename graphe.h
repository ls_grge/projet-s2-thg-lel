#ifndef GRAPHE_H_INCLUDED
#define GRAPHE_H_INCLUDED

#include <string>
#include <unordered_map>
#include "sommet.h"
#include "coords.h"
#include "arete.h"
#include <math.h>
#include <utility>


/*!
 * \file graphe.h
 * \brief Class Graphe
 * \author �milie - Lizzie - Louis
 * \version FINAL
 */


/*! \class Graphe
* \brief classe representant l'ensemble du graphe
*/

class graphe
{
    public:

          /*!<format du fichier ordre/liste des sommets/taille/liste des ar�tes*/
          /*!
         *  \brief constructeur  qui charge le graphe en m�moire
         *
         *  \param nomFichier1 : permet d'indiquer le fihier 1
         *	\param nomFichier2 : permet d'indiquer le fihier 2
         */
	       graphe(std::string nomFichier1, std::string nomFichier2);
        /*!<constructeur par d�faut*/

        /*!
        *  \brief constructeur par d�faut
        */
        graphe();

        /*!
     	*  \brief d�structeur
     	*/
        ~graphe();

	       /*!
    	  *  \brief affiche le graphe
    	  */
        void afficher() const;
        /*!
         *  \brief recherche et affiche les composantes connexes
         *  \return etourne le nombre de composantes connexes
         */
        int rechercher_CC() const;


        /*!
         * \brief cherche le plus court chemin en poids d'un sommet � un autre
         * \param Sommet*
         * \return la distance
         */
        float algoDijkstra(Sommet*) const;

        /*!
         * \brief dessin SVG
         * \param string
         */
        void dessiner(std::string);

        /*!
         * \brief dessine en svg avec les poids des aretes
         * \param svgout : objet de svgfile pour le svg
         */
        void dessinerAvecPoids(Svgfile& svgout);
         /*!
         * \brief dessine en svg avec les indice
         * \param svgout : objet de svgfile pour le svg
         */
        void dessinerAvecIndice(Svgfile& svgout);
         /*!
         * \brief dessine en svg prim
         * \param svgout : objet de svgfile pour le svg
         */
        void dessinerPrim(Svgfile& svgout);

	       /*!
         * \brief dessine en svg pareto
         * \param svgout : objet de svgfile pour le svg
         * \param sol_adm : vector qui contient toutes les solution admissible
         * \param double_poids : verifie si le poid est valide
         */
        void dessinerPareto(std::vector<std::pair<graphe, std::pair<float, float>>> &sol_adm, int num, Svgfile& svgout, bool double_poids);

         /*!
         * \brief dessine en svg le graphe des optimum
         * \param svgout : objet de svgfile pour le svg
         * \param frontiere : contient la frontiere de pareto
         */
        void dessinerGrapheOptimums(std::vector<std::pair<graphe, std::pair<float, float>>> frontiere, Svgfile& svgout);


      	/*!
      	* \brief tri les aretes en fonction du poid
      	* \param num_poids : indique quel poid trier
        * \return un vecteur d'arete*
        */
        std::vector<Arete*> Tri(int num_poids);


      	/*!
      	* \brief effectue l'algorithme de Prim
      	* \param num_poids : indique quel poid selectionner
        * \return un vecteur d'arete*
        */
        std::vector<Arete*> Prim(int num_poids);

        /*!
        * \brief affiche l'ensemble des solutions pour une optimisation d'un arbre bi-objectif selon Pareto
        */
        void optimisation_bi();

        /*!
        * \brief renvoie l'ensemble des cas possibles de bonne taille � l'algo de pareto
        * \param double_poids : verifie si le poid est valide
        * \param svgout : objet de svgfile pour le svg
        */
        void pareto(bool double_poids, Svgfile& svgout1, Svgfile& svgout2);

        /*!
        * \brief optimisation du nombre de d�part/ou d'arriv�e de la s�quence g�n�r�e valide et du nombre de fin
        * \param prend un booleen en parametre
        * \return un vector de bool
        */
        std::set<std::vector<bool>> casPossibles(bool);
        /*!
        * \brief g�n�re des combinaisons binaires avec exactement ordre-1 aretes
        * \param bool
        * \return un int
        */
        int ini(bool );

        /*!
        * \brief g�n�re des combinaisons binaires avec exactement ordre-1 aretes
        * \param vec : vector de bool
        */
        void sw_(std::vector<bool> &vec);/*!<*/

        /*!
        * \brief g�n�re des combinaisons binaires de taille >= ordre-1
        * \param vec : vector de bool
        */
        void sw(std::vector<bool> &vec);

        /*!
        * \brief g�n�re le graphe correspondant � la s�quence binaire en param�tre et le renvoie
        * \param vector de booleen
        * \return graphe
        */
        graphe converti(std::vector<bool> );

        /*!
        * \brief evaluation des objectifs pour pareto
        * \param prend un int
        * \return float
        */
        float evalObj( int);
        /*!
        * \brief renvoi les optimums de Pareto au fur et � mesure
        * \param vector : de graphe et de list
        * \return une pair de graphe et pair de float
        */
        std::pair<graphe, std::pair<float, float>> optimum(std::vector<std::pair<graphe, std::pair<float, float>>> &liste);

        /*!
        * \brief transforme un float en un string
        * \param f : float
        * \return string
        */
        std::string float2string(float f);

        /*!
        * \brief calcul les distances en fonction du poids 2
        * \return float
        */
        float evalDijkstra();

    protected:

    private:
        /// Le r�seau est constitu� d'une collection de sommets
        std::vector<Sommet*> m_sommets;
        std::vector<Arete*> m_aretes;
};

///renvoie la combinaison lin�aire correspondant � la valeur en param�tre
void initialise(int , std::vector<bool> &vec);

#endif // GRAPHE_H_INCLUDED
