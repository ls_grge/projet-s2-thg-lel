#include "arete.h"

Arete::Arete(int id, int idsommet1, int idsommet2)
    : m_id{id}, m_id_sommet_ini{idsommet1},m_id_sommet_fini{idsommet2}
{ }

Arete::Arete(int id, int idsommet1, int idsommet2, std::vector <float> poids)
    : m_id{id},m_id_sommet_ini{idsommet1},m_id_sommet_fini{idsommet2},m_poids{poids}
{ }

Arete::~Arete()
{ }

void Arete::afficher() const
{
    std::cout<<"    "<<m_id<<std::endl;
    std::cout << "         id sommet 1 : " << m_id_sommet_ini << std::endl;
    std::cout << "         id sommet 2 : " << m_id_sommet_fini << std::endl;
    for (size_t i = 0; i<m_poids.size(); ++i) {
        std::cout << "         poids "<< i << ": " << std::fixed << std::setprecision(1) << m_poids[i] << std::endl; //pour aoir un chiffre apres la virgule
    }
}

void Arete::afficherId() const
{
    std::cout<<"    "<<m_id<<std::endl;
}

void Arete::setPoids(std::vector <float> poids)
{
    for (size_t i = 0; i<poids.size(); ++i)
        m_poids.push_back(poids[i]);
}

void Arete::dessinerArete(Svgfile& svgout, std::vector<Sommet*> m_sommets, double decalage_x, double decalage_y)
{
    double x_s1, y_s1, x_s2, y_s2, x_tot, y_tot;

    x_s1 = (m_sommets[m_id_sommet_ini])->getCoords().getX();//trouver l'id du sommet //prend en fonction de l'id la deuxieme valeur
    y_s1 = (m_sommets[m_id_sommet_ini])->getCoords().getY();
    x_s2 = (m_sommets[m_id_sommet_fini])->getCoords().getX();//trouver l'id du sommet //prend en fonction de l'id la deuxieme valeur
    y_s2 = (m_sommets[m_id_sommet_fini])->getCoords().getY();
    x_tot = x_s1 + x_s2;
    y_tot = y_s1 + y_s2;

    svgout.addLine(x_s1 + decalage_x, y_s1 + decalage_y, x_s2 + decalage_x, y_s2 + decalage_y, "black");
    svgout.addText((x_tot/2) + decalage_x, (y_tot/2) + decalage_y, std::to_string(m_id), "green");
}

void Arete::dessinerAreteAvecPoids(Svgfile& svgout, std::vector< Sommet*> m_sommets, double decalage_x, double decalage_y)
{
    double x_s1, y_s1, x_s2, y_s2, x_tot, y_tot;
    std::string tot;
    x_s1 = (m_sommets[m_id_sommet_ini])->getCoords().getX();//trouver l'id du sommet //prend en fonction de l'id la deuxieme valeur
    y_s1 = (m_sommets[m_id_sommet_ini])->getCoords().getY();
    x_s2 = (m_sommets[m_id_sommet_fini])->getCoords().getX();//trouver l'id du sommet //prend en fonction de l'id la deuxieme valeur
    y_s2 = (m_sommets[m_id_sommet_fini])->getCoords().getY();
    x_tot = x_s1 + x_s2;
    y_tot = y_s1 + y_s2;

    for (size_t i = 0; i<m_poids.size(); ++i) {
        if(i %m_poids.size() != 0)
            tot += ";";
        tot = tot + float2string(m_poids[i]);
    }

    svgout.addLine(x_s1 + decalage_x, y_s1 + decalage_y, x_s2 + decalage_x, y_s2 + decalage_y, "black");
    svgout.addText((x_tot/2) + decalage_x, (y_tot/2) + decalage_y, ("(" + tot + ")"), "black");
}

std::string Arete::float2string(float f)
{
    std::ostringstream os;
    os << f;
    return os.str();
}

void Arete::setBool(bool marq)
{
    m_marq=marq;
}

bool Arete::getBool()
{
    return m_marq;
}

int Arete::getIdSommetIni()
{
    return m_id_sommet_ini;
}

int Arete::getIdSommetFin()
{
    return m_id_sommet_fini;
}


