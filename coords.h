#ifndef COORDS_H_INCLUDED
#define COORDS_H_INCLUDED

#include <iostream>

class Coords
{
    public :
        Coords(double x, double y);
        Coords();

        void afficher() const;
        void saisir();

        double getX() const;
        double getY() const;

        void setY(double y);
        void setX(double x);

    private :
        double m_x;
        double m_y;
};

#endif // COORDS_H_INCLUDED
