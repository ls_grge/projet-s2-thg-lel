#include <fstream>
#include <iostream>
#include <algorithm>
#include "graphe.h"

graphe::graphe()
{ }

graphe::graphe(std::string nomFichier1, std::string nomFichier2)
{
    std::ifstream ifs{nomFichier1};
    std::ifstream ifs2{nomFichier2};
    if (!ifs)
        throw std::runtime_error( "Impossible d'ouvrir en lecture " + nomFichier1 );

    int ordre;
    double posx;
    double posy;
    ifs >> ordre;
    if ( ifs.fail() )
        throw std::runtime_error("Probleme lecture ordre du graphe");
    int id_sommet;

    //lecture des sommets
    for (int i=0; i<ordre; ++i) {
        ifs>>id_sommet;
        if(ifs.fail())
            throw std::runtime_error("Probleme lecture donn�es sommet");
        ifs>>posx;
        if(ifs.fail())
            throw std::runtime_error("Probleme lecture donn�es posx sommet");
        ifs>>posy;
        if(ifs.fail())
            throw std::runtime_error("Probleme lecture donn�es posy sommet");
        m_sommets.push_back(new Sommet{id_sommet, {posx, posy}});
    }

    int taille;
    int id_arete;
    int id_sommet_ini;
    int id_sommet_fin;
    ifs >> taille;
    if ( ifs.fail() )
        throw std::runtime_error("Probleme lecture taille du graphe");
    //lecture des aretes
    for (int i=0; i<taille; ++i) {
        //lecture des ids des deux extr�mit�s
        ifs>>id_arete;
        if(ifs.fail())
            throw std::runtime_error("Probleme lecture donnees aretes");
        ifs>>id_sommet_ini;
        if(ifs.fail())
            throw std::runtime_error("Probleme lecture arete sommet initial");
        ifs>>id_sommet_fin;
        if(ifs.fail())
            throw std::runtime_error("Probleme lecture arete sommet final");
        //ajouter chaque extr�mit� � la liste des voisins de l'autre (graphe non orient�)
        m_aretes.push_back(new Arete{id_arete,id_sommet_ini, id_sommet_fin}); //0 par defaut puis valeur modifi�e � la lecture de l'autre
        ///ajout des vosins de part et d'autre de l'aret
        m_sommets[id_sommet_ini]->setVoisins(m_sommets[id_sommet_fin]);
        m_sommets[id_sommet_fin]->setVoisins(m_sommets[id_sommet_ini]);
    }

    if (!ifs2)
        throw std::runtime_error( "Impossible d'ouvrir en lecture " + nomFichier2 );

    ifs2 >> taille;
    if ( ifs2.fail() )
        throw std::runtime_error("Probleme lecture taille du graphe");
    size_t num;

    ifs2>>num;
    if(ifs2.fail())
        throw std::runtime_error("Probleme lecture nombre de ponderation");
    std::vector<float> poids(num);
    //lecture des sommets
    for (int i=0; i<taille; ++i) {
        ifs2>>id_arete;
        for (size_t j=0; j<num; ++j) {
            float var;
            ifs2>>var;
            if(ifs2.fail())
                throw std::runtime_error("Probleme lecture poids de l'arete");
            poids[j]=var;
        }

        (m_aretes[id_arete])->setPoids(poids);
        ///ajout des poids
        m_sommets[m_aretes[id_arete]->getSommets().first]->setPoids(m_aretes[id_arete]->getSommets().second, poids[1]);
        m_sommets[m_aretes[id_arete]->getSommets().second]->setPoids(m_aretes[id_arete]->getSommets().first, poids[1]);
    }
}

void graphe::afficher() const
{
    std::cout<<"graphe : "<<std::endl;
    std::cout<<"  ordre : "<< m_sommets.size() <<std::endl;

    for(auto s:m_sommets){
        std::cout <<"  sommet :" ;
        s->afficherData();
        s->afficherVoisins();
        //(s.second)->afficherVoisins();
        std::cout<<std::endl<<std::endl;
    }
    std::cout<<"  taille : "<< m_aretes.size() <<std::endl;
    for(auto a:m_aretes) {
        std::cout <<"  arete :" ;
        a->afficher();
        //(s.second)->afficherVoisins();
        std::cout<<std::endl<<std::endl;
    }
}

void graphe::dessinerAvecIndice(Svgfile& svgout)
{
    for (auto s:m_sommets)
        s->dessinerSommet(svgout, 0, 0);

    for(auto a:m_aretes)
        a->dessinerArete(svgout, m_sommets, 0, 0);
}

void graphe::dessinerAvecPoids(Svgfile& svgout)
{
    for (auto s:m_sommets)
        s->dessinerSommet(svgout, 400, 0);

    for(auto a:m_aretes)
        a->dessinerAreteAvecPoids(svgout, m_sommets, 400, 0);
}

void graphe::dessiner(std::string nom_graphe)
{
    Svgfile svgout{"etape1.svg"};

    ///GRAPHE SANS MODIFICATIONS ///
    svgout.addText(350,35,nom_graphe,"black");
    svgout.addText(350,50,"GRAPHE DE DEPART","black");
    dessinerAvecIndice(svgout);
    dessinerAvecPoids(svgout);

    ///GRAPHE APRES PRIM MODIFICATIONS ///
    svgout.addText(350,450,"EN APPLIQUANT PRIM","black");
    dessinerPrim(svgout);
}

void graphe::dessinerPrim(Svgfile& svgout)
{
    std::vector<Arete*> aretes_prim;
    std::string total;
    std::string total_string;

    int pond = m_aretes[0]->getPond();
    std::vector<float> cout_total(pond);

    for (int i = 0; i<pond; ++i) {
        aretes_prim = Prim(i);

        for (auto s:m_sommets)
            s->dessinerSommet(svgout, i*400, 400);

        for(auto a:aretes_prim)
            a->dessinerAreteAvecPoids(svgout, m_sommets, i*400, 400);

        for (auto a : aretes_prim) {
            //cout total pour faire les 2
            for (int j = 0; j<pond; j++)
                cout_total[j]+=a->GetPoids(j);
        }

        for (size_t e = 0; e<cout_total.size(); e++) {
            if (e%cout_total.size()!=0)
                total += ";";
            total = total + float2string(cout_total[e]);
        }
        svgout.addText(200 + i*400, 450, ("cout : ("+total+")"),"green");
        total = "";

        //on reinisialise le vecteur de poids a zero
        for (int j = 0; j<pond; j++)
            cout_total[j]=0;
    }
}

std::vector<Arete*> graphe::Tri(int num_poids)
{
    std::vector<Arete*> tmp_m_aretes = m_aretes;

    ///Tri par ordre croissant de poids
    std::sort(tmp_m_aretes.begin(), tmp_m_aretes.end(), [num_poids](Arete* arete1, Arete* arete2) {
        return arete1->GetPoids(num_poids) < arete2->GetPoids(num_poids);
    });

    return tmp_m_aretes;
}

std::vector<Arete*> graphe::Prim(int num_poids)
{
    std::vector<Arete*> arete_ok;
    std::vector<Arete*> tmp_m_aretes = Tri(num_poids);

    ///TRI
    std::cout << "TRI" << std::endl;
    /*for (auto a:tmp_m_aretes)
        a->afficherId();*/

    ///DEBUT ALGO

    /// ARRETE1
    arete_ok.push_back(tmp_m_aretes[0]);
    tmp_m_aretes[0]->setBool(true);
    m_sommets[tmp_m_aretes[0]->getIdSommetIni()]->setBool(true);
    m_sommets[tmp_m_aretes[0]->getIdSommetFin()]->setBool(true);

    /// LES AUTRES
    int arret = 0;
    int S1, S2;

    for (size_t i= 0; i <(m_sommets.size()-2); ++i) {

        for (size_t j = 0; j<tmp_m_aretes.size(); ++j) {
            S1 = tmp_m_aretes[j]->getIdSommetIni();
            S2 = tmp_m_aretes[j]->getIdSommetFin();

            if (tmp_m_aretes[j]->getBool()==false && arret == 0) {
                if(m_sommets[S1]->getBool()==false && m_sommets[S2]->getBool()==true) { //S2 marque
                    m_sommets[S1]->setBool(true);
                    tmp_m_aretes[j]->setBool(true);
                    arete_ok.push_back(tmp_m_aretes[j]);
                    arret=1;
                }

                else if(m_sommets[S1]->getBool()==true && m_sommets[S2]->getBool()==false) { //S1 marque
                    m_sommets[S2]->setBool(true);
                    tmp_m_aretes[j]->setBool(true);
                    arete_ok.push_back(tmp_m_aretes[j]);
                    arret=1;
                }
            }
        }
        arret=0;
    }

    std::cout << "PRIM" << std::endl;
    /*for (auto a : arete_ok)
        a->afficherId();*/

    for (auto s : m_sommets)
        s->setBool(false);

    for (auto a : m_aretes)
        a->setBool(false);

    return arete_ok;
}


std::string graphe::float2string(float f)
{
    std::ostringstream os;
    os << f;
    return os.str();
}

int graphe::rechercher_CC() const
{
    int i=0; ///nombre de composante connexe
    std::unordered_set<int> copie; ///contient les sommets non d�couverts
    std::unordered_set<int> cc; ///contient les somments d'une composante connexe
    std::unordered_set<int>::iterator it; ///it�rateur

    for(auto s:m_sommets)
        copie.insert(s->getId());

    while(copie.size()!=0)
    {
        Sommet*s0;
        ++i; ///on incr�mente le nombre de composante connexe
        if(i>1) return 2;

        ///recherche une composante � partir d'un sommet
        it= copie.begin();
        s0=m_sommets[*it];
        cc = s0->rechercherCC();

        for (auto s:cc) {
            ///on cherche les sommets de la composante pour le supprimer de la liste
            if (copie.find(s) != copie.end())
                copie.erase(s);
        }
    }
    return i;
}

graphe::~graphe()
{
    //dtor
}

float graphe::algoDijkstra(Sommet* s0) const
{
    std::unordered_map<Sommet*,std::pair<Sommet*, float>> l_pred;
    std::unordered_map<Sommet*,std::pair<Sommet*, float>>::iterator it;
    float distance=0.0;

    l_pred=s0->algoDijkstra();

    for(auto c: l_pred) ///ajout de toute les valeurs de poids 2 : poids2 total pour un graphe au d�part de s0
        distance+= c.second.second;

    return distance;
}

float graphe::evalDijkstra(){

    /**On lance dijkstra depuis un sommet*/
    ///ici exemple avec sommet id 0 du graphe de d�part
    for (size_t i =0; i< m_sommets.size(); ++i)
    {
        algoDijkstra(m_sommets[i]);
    }

    float d=0;
    for (auto s: m_sommets)
        d+=algoDijkstra(s);

    //std::cout<< "\n  DISTANCE (POIDS2) TOTALE GRAPHE  " << d << std::endl;

    return d;
}

void graphe::optimisation_bi(){

    Svgfile svgout_2P{"etape2_pareto.svg"}, svgout_3P{"etape3_pareto.svg"};
    Svgfile svgout_2GF{"etape2_graphefrontiere.svg"}, svgout_3GF{"etape3_graphefrontiere.svg"};

    std::cout << " OPTIMISATION COUTS " << std::endl << std::endl;
    ///Pareto bi-objectif cout1 cout2
    pareto(true, svgout_2P, svgout_2GF);

    std::cout << " OPIMISATION COUT-DISTANCE " << std::endl << std::endl;
    ///Pareto bi-objectif cout / distance
    pareto(false, svgout_3P, svgout_3GF);
}

void graphe::pareto(bool double_poids, Svgfile& svgout1, Svgfile& svgout2) {
    ///ensemble des solutions possible de taile (ordre-1)
    std::set<std::vector<bool>> sol;
    ///ensemble des solutions admissibles
    std::vector<std::pair<graphe, std::pair<float, float>>> sol_adm;
    ///ensemble des solutions non domin�es = fronti�re de Pareto
    std::vector<std::pair<graphe, std::pair<float, float>>> frontiere;
    int obj1=0, obj2=1;
    graphe g;

    sol=casPossibles(double_poids);

    std::cout << "  CAS POSSIBLES DE BONNE TAILLE OK " << sol.size() << std::endl;

    for (auto c : sol) {
        g = converti(c);

        ///connexite ?
        if (g.rechercher_CC()==1) {
            //std::cout << " INSERT  " << std::endl;
            if(double_poids==true) ///selon les couts
               sol_adm.push_back({g, {g.evalObj(obj1), g.evalObj(obj2)}} );
            else ///selon cout / distance
                sol_adm.push_back({g, {g.evalObj(obj1), g.evalDijkstra()}} );
        }
    }

    std::cout << "\n\n  NOMBRE DE LA VALEUR ADMISSIBLE " << sol_adm.size() << std::endl;
    /// ensemble des soltuions admissibles = solution domin�es + non domin�es

    /**DESSINER GRAPHE DES SOL ADMISSIBLE**/

    dessinerPareto(sol_adm, 1, svgout1, double_poids);

    std::cout<<"\n\n DEBUT FRONTIERE DE PARETO ****************************" <<std::endl;

    ///Tri par ordre croissant en fonction de obj2
    std::sort(sol_adm.begin(), sol_adm.end(), [](std::pair<graphe, std::pair<float, float>> p1, std::pair<graphe, std::pair<float, float>> p2) {
        return p1.second.second <= p2.second.second;
    });
    ///Tri par ordre croissant en fonction de obj1
    std::sort(sol_adm.begin(), sol_adm.end(), [](std::pair<graphe, std::pair<float, float>> p1, std::pair<graphe, std::pair<float, float>> p2) {
        return p1.second.first < p2.second.first;
    });

    std::cout<<"\n\n SELECTION OPTIMUM" <<std::endl;

    while(!sol_adm.empty()) {
        frontiere.push_back(optimum(sol_adm));
        //std::cout << " INSERT  " << std::endl;
    }

    std::cout<<"\n\n FIN FRONTIERE DE PARETO " << frontiere.size() << "   ****************************" <<std::endl;

    /** DESSINER GRAPHE AVEC FRONTIERE DE PARETO **/
    dessinerPareto(frontiere, 2, svgout1, double_poids);

    ///DESSINER LES GRAPHES CORREPONDANTS A LA FRONTIERE///
    dessinerGrapheOptimums(frontiere, svgout2);
}

void graphe::dessinerGrapheOptimums(std::vector<std::pair<graphe, std::pair<float, float>>> frontiere, Svgfile& svgout){

    for (size_t i = 0; i<frontiere.size(); ++i) {
        int esp = 400;
        int taille_s = (frontiere[i].first).m_sommets.size();
        int taille_a = (frontiere[i].first).m_aretes.size();
        std::string affi = "Solution " + std::to_string(i);
        std::string affi2 = "(" + float2string((frontiere[i].second).first) + ";" + float2string((frontiere[i].second).second) + ")";

        if (i<3) {
            svgout.addText (i*esp+225,50, affi,"black");
            svgout.addText (i*esp+225,70, affi2,"black");
            for (int j = 0; j<taille_s; j++)
                (frontiere[i].first).m_sommets[j]->dessinerSommet(svgout, i*esp, i);

            for (int j = 0; j<taille_a; j++)
                (frontiere[i].first).m_aretes[j]->dessinerArete(svgout, frontiere[i].first.m_sommets, i*esp, i);
        }

        if (i>=3 && i<6) {
            svgout.addText (i*esp+225 - 3*esp, 50 +esp, affi,"black");
            svgout.addText (i*esp+225 - 3*esp, 70 +esp, affi2,"black");
            for (int j = 0; j<taille_s; j++)
                (frontiere[i].first).m_sommets[j]->dessinerSommet(svgout, i*esp - 3*esp, i+esp);

            for (int j = 0; j<taille_a; j++)
                (frontiere[i].first).m_aretes[j]->dessinerArete(svgout, frontiere[i].first.m_sommets, i*esp - 3*esp, i+esp);
        }

        if (i>=6 && i<9) {
            svgout.addText (i*esp+225 - 6*esp, 50 +(2*esp), affi,"black");
            svgout.addText (i*esp+225 - 6*esp, 70 +(2*esp), affi2,"black");
            for (int j = 0; j<taille_s; j++)
                (frontiere[i].first).m_sommets[j]->dessinerSommet(svgout, i*esp - 6*esp, i+(2*esp));

            for (int j = 0; j<taille_a; j++)
                (frontiere[i].first).m_aretes[j]->dessinerArete(svgout, frontiere[i].first.m_sommets, i*esp - 6*esp, i+(2*esp));
        }

        if (i>=9 && i<12) {
            svgout.addText (i*esp+225 - 9*esp, 50 +(3*esp), affi,"black");
            svgout.addText (i*esp+225 - 9*esp, 70 +(3*esp), affi2,"black");
            for (int j = 0; j<taille_s; j++)
                (frontiere[i].first).m_sommets[j]->dessinerSommet(svgout, i*esp - 9*esp, i+(3*esp));

            for (int j = 0; j<taille_a; j++)
                (frontiere[i].first).m_aretes[j]->dessinerArete(svgout, frontiere[i].first.m_sommets, i*esp - 9*esp, i+(3*esp));
        }
    }
}

std::pair<graphe, std::pair<float, float>> graphe::optimum(std::vector<std::pair<graphe, std::pair<float, float>>> &liste)
{
    ///on recupere le premier element
    std::pair<graphe, std::pair<float, float>> p=liste[0];
    liste.erase(liste.begin());

    for(unsigned int i=0; i<liste.size(); ++i) {
        ///on parcourt et si obj2 >= � obj2 du top alors on supprime
        if((liste[i].second.second)>=p.second.second) {
            liste.erase(liste.begin() + i);
            //std::cout << " ERASE  " << std::endl;
            --i;
        }
    }

    return p;
}


std::set<std::vector<bool>> graphe::casPossibles(bool double_poids)
{
    std::set<std::vector<bool>> cas;
    std::set<std::vector<bool>>::iterator it;
    std::vector<bool> sequence, fin; //combinaison binaire

    int taille =m_aretes.size();

    for(int i=0; i<taille ; ++i) {
        sequence.push_back(false);
        fin.push_back(false);
    }

    initialise(ini(true), sequence);

    if(double_poids==true)
        initialise(ini(false), fin);
    else
        initialise(pow(2, taille)-1, fin);

    cas.insert(sequence);

    int cmpt =0;
    do {
        ++cmpt;
        if(double_poids==true)
            sw_(sequence);
        else
            sw(sequence);

        cas.insert(sequence);
    }
    while (sequence!=fin);

    std::cout << "\n  nombre de valeur generees : " << cmpt << std::endl;
    return cas;
}

int graphe::ini(bool debut)
{
    int somme =0;

    if (debut==true) /// somme de n=1 � ordre de 2^(ordre-n)
        for(unsigned int i=0; i<m_sommets.size()-1; ++i)
            somme+=pow(2, i);
    else /// somme de n=1 � ordre de 2^(taille-1-n)
        for(unsigned int i=0; i<m_sommets.size()-1; ++i)
            somme+=pow(2, m_aretes.size() -1-i );

    std::cout << " somme " << somme ;

    return somme;
}

void initialise(int valeur, std::vector<bool> &vec)
{
    int taille= vec.size();
    int cmpt=0;
    do {
        if ((valeur- pow(2, taille-cmpt-1)) >= 0) {
            vec[cmpt]=true;
            valeur -= pow(2, taille-cmpt-1) ;
        }
        ++cmpt;
    }
    while (cmpt!=taille);

    std::cout << "  sequence  " ;
    for ( int j=0; j<taille; ++j)
        std::cout << vec.at(j) ;
    std::cout << "   " << std::endl;
}

void graphe::sw_( std::vector<bool> &vec)
{
    int taille = vec.size();
    int i=taille-1;

    if (vec[i]==true) { ///ex 011 --> 101 OU 1001 --> 1010
        while(vec[i-1] ==true) {
            --i;
        };
        vec[i]=false;
        vec[i-1]=true;
    }
    else  { ///(vec[i]==false)
        int cmpt=0;
        while(vec[i-1]==false)
            --i;
        while(vec[i-1]==true) {
            cmpt++;
            --i;
        }
        vec[i-1]=true;
        --cmpt;

        while(i!=taille-cmpt) {
            vec[i]=false;
            ++i;
        }
        while(i!=taille) {
            vec[i]=true;
            ++i;
            --cmpt;
        }
    }
}

void graphe::sw(std::vector<bool> &vec)
{
    int ordre=m_sommets.size();

    int taille = vec.size();
    int i=taille-1;
    int cmpt_tot=0, cmpt=0;

    for(auto b:vec) ///compte le nombre de '1' dans le vecteur binaire
        if(b==true)     ++cmpt_tot;

    if(vec[i]==false){ ///ex 01110 --> 01111
        vec[i]=true;
    }
    else if(cmpt_tot==(ordre-1)){ ///ex 00111 --> 01011
        while(vec[i-1]==true)  {
            cmpt++;
            --i;
        }
        vec[i-1]=true;
        while(i!=taille-cmpt){
            vec[i]=false;
            ++i;
        }
        while(i!=taille){
            vec[i]=true;
            ++i;
            --cmpt;
        }
    }
    else { ///ex 01111 --> 10011
        while(vec[i-1] ==true){
            vec[i]=false;
            --i;
        };
        vec[i]=false;
        vec[i-1]=true;
        ++i;
        i=taille-1;

        while(i!= taille-(ordre-1)){
            vec[i]=true;
            --i;
        }
    }
}

graphe graphe::converti(std::vector<bool> vec)
{
    int taille= vec.size();
    graphe g;

    ///copier la liste de tous les sommets : g est de m�me ordre que le graphe de d�part
    for (auto s:this->m_sommets)    {
        g.m_sommets.push_back(new Sommet{s->getId(), s->getCoords()});
    }

    ///refaire la liste des aretes
    ///si true alors arete existe
    for(auto i=0; i< taille; ++i) {
        if (vec[taille-1-i]==true) {
            g.m_aretes.push_back(this->m_aretes[i]);
            g.m_sommets[this->m_aretes[i]->getSommets().first]->setVP(g.m_sommets[this->m_aretes[i]->getSommets().second],
                    (this->m_sommets[this->m_aretes[i]->getSommets().first])
                    ->getPoids2(this->m_aretes[i]->getSommets().second));
            g.m_sommets[this->m_aretes[i]->getSommets().second]->setVP(g.m_sommets[this->m_aretes[i]->getSommets().first],
                    (this->m_sommets[this->m_aretes[i]->getSommets().second])->getPoids2(this->m_aretes[i]->getSommets().first));
        }
    }

    return g;
}

float graphe::evalObj(int obj)
{
    float somme =0;

    for(auto a:m_aretes)
        somme+= a->GetPoids(obj);

    return somme;
}

void graphe::dessinerPareto(std::vector<std::pair<graphe, std::pair<float, float>>> &sol_adm, int num, Svgfile& svgout, bool double_poids)
{
    if((num == 1) && (double_poids == true)) {
        svgout.addGrid2();

        ///les axes
        svgout.addLine(100, 6550, 1400, 6550, "grey");
        svgout.addLine(100, 6550, 100, 100, "grey");

        ///fleches hautes
        svgout.addLine(100, 100, 85, 125, "grey");
        svgout.addLine(100, 100, 115, 125, "grey");

        ///fleches bas
        svgout.addLine(1400, 6550, 1375, 6565, "grey");
        svgout.addLine(1400, 6550, 1375, 6535, "grey");

        ///nom cout
        svgout.addText(110, 100, "COUT 2", "black");
        svgout.addText(1410, 6550, "COUT 1", "black");

        for (size_t i = 0; i<sol_adm.size(); ++i)
            svgout.addDisk((double)sol_adm[i].second.first*10 + 100, 6550 -(double)sol_adm[i].second.second*10, 5, "red");
    }

    if ((num==2) && (double_poids == true)) {
        for (size_t i = 0; i<sol_adm.size(); ++i) {
            svgout.addDisk((double)sol_adm[i].second.first*10 + 100, 6550-(double)sol_adm[i].second.second*10, 5, "green");
            svgout.addText((double)sol_adm[i].second.first*10 + 100, 6550 -(double)sol_adm[i].second.second*10, std::to_string (i), "black");
        }
    }


    /**pour cout 1 & distance **/
    if((num == 1) && (double_poids == false)) {
        svgout.addGrid3();

        ///les axes
        svgout.addLine(100, 6550, 1400, 6550, "grey");
        svgout.addLine(100, 6550, 100, 100, "grey");

        ///fleches hautes
        svgout.addLine(100, 100, 85, 125, "grey");
        svgout.addLine(100, 100, 115, 125, "grey");

        ///fleches bas
        svgout.addLine(1400, 6550, 1375, 6565, "grey");
        svgout.addLine(1400, 6550, 1375, 6535, "grey");

        ///nom cout
        svgout.addText(110, 100, "COUT 2", "black");
        svgout.addText(1410, 6550, "COUT 1", "black");

        for (size_t i = 0; i<sol_adm.size(); ++i)
            svgout.addDisk((double)sol_adm[i].second.first*10 + 100, 6550 -(double)sol_adm[i].second.second*1, 5, "red");
    }

    if ((num==2) && (double_poids == false)) {
        for (size_t i = 0; i<sol_adm.size(); ++i) {
            svgout.addDisk((double)sol_adm[i].second.first*10 + 100, 6550-(double)sol_adm[i].second.second*1, 5, "green");
            svgout.addText((double)sol_adm[i].second.first*10 + 100, 6550 -(double)sol_adm[i].second.second*1, std::to_string (i), "black");
        }
    }
}
