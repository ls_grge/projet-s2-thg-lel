#include <iostream>
#include "graphe.h"
#include "arete.h"
//#include <ctime>


int main()
{
  //  clock_t clk = clock();
    graphe g{"manhattan.txt","manhattan_weights_0.txt"};
    //graphe g{"broadway.txt","broadway_weights_0.txt"};
    //graphe g{"triville.txt","triville_weights_0.txt"};
    //graphe g{"cubetown.txt","cubetown_weights_0.txt"};

    g.optimisation_bi();

    g.dessiner("manhattan");  //poids

    //std::cout << std::endl << "Temps d execution : " << clk;
    return 0;
}
